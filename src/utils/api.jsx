var Fetch = require("whatwg-fetch");
var RootUrl = "https://api.imgur.com/3/"
var ApiKey = "8bc7286f8c24875"

module.exports = {
  get: function(url) {
    return fetch(RootUrl + url, {
      headers: {
        'Authorization': 'Client-ID ' + ApiKey
      }
    })
    .then(function(response){
      return response.json()
    })
  }
};